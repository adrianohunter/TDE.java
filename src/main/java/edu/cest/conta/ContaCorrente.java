package edu.cest.conta;

import java.util.Random;

public class ContaCorrente {

	/**
	 * TODO - Corrija deixando os atributos protegidos e acessiveis somente de
	 * acordo com os padroes
	 */
	private Random rand = new Random();
	private double saldo = 0;
	private double saldoChequeEspecial = rand.nextDouble() * 1000; // NAO ALTERE ESSA CHAMADA
	private String numeroConta;

	/**
	 * TODO - Implemente o saque para subtrair o valor do saldo. TODO -
	 * Aten&ccedil;&atilde;o o saque s&oacute; pode ser efetuado se tiver saldo
	 * suficiente na conta <i>MAS</i>, verifique o cheque especial
	 * 
	 * @return - Saldo apos o saque
	 */
	public double saque(String numeroConta, double valorSaque) {
		// TODO - Me implemente corretamente
		if (this.numeroConta == numeroConta) {
			if (this.saldo > 0) {
				this.saldo = saldo - valorSaque;
				System.out.println("Saldo: " + saldo);
			} else if (this.saldo == 0 && saldoChequeEspecial > 0) {
				this.saldoChequeEspecial = saldoChequeEspecial - valorSaque;
				System.out.println(
						"Saldo: " + this.saldo + " " + "Saldo do Cheque Especial: " + this.saldoChequeEspecial);
			} else {
				System.out.println("O seu saldo � insuficiente para realizar o saque");
			}
		}

		return 0;
	}

	/**
	 * TODO - Implemente o dep&oacute;sito TODO - Aten&ccedil;&atilde;o o
	 * dep&oacute;sito n&atilde;o pode ser de valor negativo
	 */
	public double deposito(String numeroConta, double valorDeposito) {
		// TODO - Me implemente corretamente
		if (this.numeroConta == numeroConta) {
			this.saldo = saldo + valorDeposito;
			System.out.println("Seu saldo ap�s o deposito que acabou de ser realizado: R$" + this.saldo);
		}
		return 0;
	}

	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	public double getSaldoChequeEspecial() {
		return saldoChequeEspecial;
	}

	public void setSaldoChequeEspecial(double saldoChequeEspecial) {
		this.saldoChequeEspecial = saldoChequeEspecial;
	}

	public String getNumeroConta() {
		return numeroConta;
	}

	public void setNumeroConta(String numeroConta) {
		this.numeroConta = numeroConta;
	}

	public void setSaldo(int i, int j) {
		// TODO Auto-generated method stub
		
	}

}
