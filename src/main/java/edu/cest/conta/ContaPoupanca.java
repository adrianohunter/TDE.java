package edu.cest.conta;

public class ContaPoupanca {
	/**
	 *  TODO - Corrija deixando os atributos protegidos e acessiveis somente de
	 *  acordo com os padroes
	 */
	private double saldo = 0;
	private String numeroConta;
	
	/**
	 * TODO - Implemente o saque para subtrair o valor do saldo.
	 * TODO - Atenção o saque só pode ser efetuado se tiver saldo suficiente na conta
	 * @return - Saldo apos o saque
	 */
	public double saque(String numeroConta, double valorSaque ) {
		
		if (this.numeroConta == numeroConta && valorSaque <= saldo) {
			if (this.saldo > 0) {
				this.saldo = saldo - valorSaque;
				System.out.println("Saldo: " + saldo);
			} else {
				System.out.println("O seu saldo � insuficiente para realizar o saque");
	

		//TODO - Me implemente corretamente
		
		return this.saldo;
	}
			
	
	/**
	 * TODO - Implemente o depósito
	 * TODO - Atenção o depósito não pode ser de falor negativo
	 */
	public double deposito( String numeroConta, double valorDeposito ) {
		//TODO - Me implemente corretamente
		
		if (this.numeroConta == numeroConta && valorDeposito >0) {
			this.saldo = saldo + valorDeposito;
			System.out.println("Seu saldo ap�s o deposito que acabou de ser realizado: R$" + this.saldo);
		}
		return this.saldo;
	}

	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	public String getNumeroConta() {
		return numeroConta;
	}

	public void setNumeroConta(String numeroConta) {
		this.numeroConta = numeroConta;
	}

	
	
	
	
}
	
	
	
