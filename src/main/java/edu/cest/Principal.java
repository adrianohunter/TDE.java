package edu.cest;

import java.util.ArrayList;


import edu.cest.cadastro.Agencia;
import edu.cest.conta.ContaCorrente;
import edu.cest.conta.ContaPoupanca;

/**
 * Classe para inicializacao do projeto,
 * com o objetivo de rodar a aplicacao via console
 * @author jwalker
 *
 */
public class Principal {
	//TODO - Implemente o inicio do programa
	//TODO - Crie duas ou mais agencias
	//TODO - Insira contas completas
	//TODO - Corrija os pacotes onde estiver errado
	//TODO - Corrija o que for necessário para atender os padrões
	//TODO - Ao fim imprima o total do saldo de todas as contas de cada agencia, seguindo os padrões já explicados em sala
	
	public static void main(String[] args) {
		Agencia agn1 = new Agencia("0337");
		Agencia agn2 = new Agencia("0368");
		
		ArrayList<Agencia> listaAgencia = new ArrayList<Agencia>();
		
		ContaCorrente cc = new ContaCorrente();
		cc.setNumeroConta("1111");
		cc.setSaldo(500,00);
		
		ContaCorrente cc2 = new ContaCorrente();
		cc2.setNumeroConta("2222");
		cc.setSaldo(150,00);
		
	    ContaPoupanca cp = new ContaPoupanca();
	    cp.setNumeroConta("3333");
	    cp.setSaldo(400);

	    ContaPoupanca cp2 = new ContaPoupanca();
	    cp2.setNumeroConta("4444");
	    cp2.setSaldo(100);
	    
	    
	    ArrayList<ContaCorrente> listaCCAge2 = new ArrayList<ContaCorrente>();
	    listaCCAge2.add(cc2);
	    agn2.setNumeroDeCC(agn2.getNumeroDeCC() + 1);
	    agn2.setListaCC(listaCCAge2);
	   
	    ArrayList<ContaCorrente> listaCC = new ArrayList<ContaCorrente>();
		listaCC.add(cc);
		agn1.setNumeroDeCC(agn1.getNumeroDeCC() + 1);
		agn1.setListaCC(listaCC);
		
		ArrayList<ContaPoupanca> listaCP = new ArrayList<ContaPoupanca>();
		listaCP.add(cp);
		agn2.setNumeroDeCP(agn2.getNumeroDeCP() + 1);
		agn2.setListaCP(listaCP);
		
		listaAgencia.add(agn1);
		listaAgencia.add(agn2);
		
		
		
		
		// TODO - Implemente de modo que haja uma lista das agencias
		// TODO - Implemente de modo que o System.out ao chamar o objeto agencia, imprima os dados como Numero da Agencia e quantidade de contas
		for (Agencia age: listaAgencia) {
			System.out.println("Agencia - Dados");
			System.out.println(age.getCodAgencia());
			System.out.println("Qnt. de contas correntes: ");
			System.out.println(age.getNumeroDeCC());
			System.out.println("Qnt. de contas poupanca: ");
			System.out.println(age.getNumeroDeCP());
		



		}
	}
}
